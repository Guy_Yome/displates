# Import libraries.
import os
import time
import webbrowser

# Below is a link to download the project to avoid writing this all by hand.
# https://gitlab.com/Guy_Yome/displates/-/tree/master/Polish_Cow

# This will open the music video associated with what's happening in the console.
webbrowser.open("https://www.youtube.com/watch?v=Vy8moBcKVIM")

insane_cow_moves = []
for insane_cow_move in range(4):
	insane_cow_moves.append(open(str(insane_cow_move) + ".txt", "r").read())

annihilate_the_cow_but_not_for_too_long = lambda: os.system('clear')

look_at_it_go = 0
this_song_rules = True
while this_song_rules:
	annihilate_the_cow_but_not_for_too_long()

	print(insane_cow_moves[look_at_it_go])
	look_at_it_go += 1

	if look_at_it_go > 3:
		look_at_it_go = 0

	time.sleep(0.41)


#                                              .**,,.                            
#                                              *##(               .,*.           
#                                            .#&@@#.  .,,,,.   ./*               
#                                         *&@@@@@#*/*,,,,,,,,,,/%(               
#                                             .//(##%%*,,,,,,,,,,.               
#                                            /&#((##(*,,,,,,,,,,,,.              
#                                         /&@@@#((#%%%(////(/*,,,,,*,,           
#                               /((((&%#&@@@@@@%&@@@@@&%#(((((((#%&&&&&%,        
#                ,,*#&@@@&#*(&@@@@@@@@@@@@@@@@@&@@@@&&%%#####%%&&&&&&&&/         
#           ,*@@@@@@@@&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@&&&&&&&#(//////,             
#          /*/@@@@@@@@@@@#/*(&@@@@@@@@@@@@(/#@@@@@@@@@@@@&*                      
#         /#%@@@@@@@@@@@%,..*&@@@@@@@@@@@&*.,#@@@@@@@@@@@/                       
#        .&@@@@@@@@@@@@#...,%@@@@@@@@@@%*....(@@@@@@@@@@@@/                      
#       .%@@@@@@@@@@@@#**/#@@@@@@@@@@@#**#&&@@@@@@@@@@@@@@@#.                    
#       .&@@@@@@@@@@@@&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/                    
#       .&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#.                   
#       .##&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@(.                    
#       .##%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%%*                     
#        .#%%%&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#///((#,                   
#        .#%%%##&@@@@@@@@&&%%&@@@@@@@@@@@@@@@@@@@&%,  ,(((//(#/                  
#        .%%&%  *&&&&&&&&&%%%%&@@@@&&&@@@@@@(*.          ./(///(,                
#       .#%&%.             . ..(##(((#%*.                   *((///*.             
#        .%%#.                .(##((###*.                     .*((////,          
#         #%(                   (#((##//*                         *#(//*/*       
#         ((                     ##((#///**                          /(((((##*   
#         #(/                    (##(*  */***,                                   
#         ***,                    (((,    ,/***,                                 
#         *#(,                    //(,                                           
#                                #(/(,                                           
#                                 ((/#*                                          
#                                 ##(#%%*                                        
#                                  *                                             